#!/bin/sh

# Access folder
script_path=$(readlink -f "${0}")
test_path=$(readlink -f "${script_path%/*}")
cd "${test_path}/"

# Configure tests
set -ex

# Run tests
gitlabci-local </dev/null && exit 1 || true
gitlabci-local -p
gitlabci-local -H --all </dev/null
gitlabci-local -H -q -p
gcil -H -p
timeout 5 gitlabci-local 'Job 1' -H --sockets
timeout 5 gitlabci-local 'Job 1' --bash && exit 1 || true
timeout 5 gitlabci-local 'Job 1' --debug && exit 1 || true
! type sudo >/dev/null 2>&1 || (sudo -E env PYTHONPATH="${PYTHONPATH}" timeout 5 gitlabci-local 'Job 1' --debug && exit 1 || true)
gcil -H Job
gcil -H 1
gcil -H 4 && exit 1 || true
